# Resmi Go Docker görüntüsünü temel alın
FROM golang:1.16-alpine

# Uygulama kaynak kodunu Docker içine kopyalayın
COPY . /app

# Uygulama dizinine gidin
WORKDIR /app

# Uygulamayı derleyin
RUN go build -o main .

# Uygulamayı çalıştırın
CMD ["./main"]