package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		_, _ = writer.Write([]byte(fmt.Sprintf("Requested URL: %s", request.URL)))
	})
	_ = http.ListenAndServe(":8080", nil)
}
